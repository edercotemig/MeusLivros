//
//  ViewController.swift
//  MeusLivros
//
//  Created by Eder Andrade on 08/09/22.
//

import UIKit

struct Livro {
    let nome: String
    let autor: String
    let editora: String
    let nomeImagemPequena: String
    let nomeImagemGrande: String
}

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
   
    var listaDeLivros: [Livro] = []
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.listaDeLivros.append(Livro(nome: "Guia dos Mochileiros das Galáxias", autor: "Douglas Adams", editora: "Editora Arqueiro", nomeImagemPequena: "capa_guia_pequeno", nomeImagemGrande: "capa_guia_grande"))
        
        self.listaDeLivros.append(Livro(nome: "O Menino Maluquinho", autor: "Ziraldo", editora: "Editora Melhoramentos", nomeImagemPequena: "capa_menino_pequeno", nomeImagemGrande: "capa_menino_grande"))
        
        self.listaDeLivros.append(Livro(nome: "1984", autor: "George Orwell", editora: "Editora Companhia das Letras", nomeImagemPequena: "capa_1984_pequeno", nomeImagemGrande: "capa_1984_grande"))
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaDeLivros.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelulaLivro", for: indexPath) as! MyCellBook
        let livro = self.listaDeLivros[indexPath.row]
        
        cell.nome.text = livro.nome
        cell.autor.text = livro.autor
        cell.editora.text = livro.editora
        cell.capa.image = UIImage(named: livro.nomeImagemPequena)
        
        return cell
    } 

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "abrirDetalheLivro", sender: indexPath.row)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let detalhesViewController = segue.destination as! DetalheLivroViewController
        let indice = sender as! Int
        let livro = self.listaDeLivros[indice]
        
        detalhesViewController.nomeLivro = livro.nome
        detalhesViewController.nomeAutor = livro.autor
        detalhesViewController.nomeEditora = livro.editora
        detalhesViewController.nomeImage = livro.nomeImagemGrande
    }
}

