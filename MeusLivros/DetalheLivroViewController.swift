//
//  DetalheLivroViewController.swift
//  MeusLivros
//
//  Created by Eder Andrade on 08/09/22.
//

import UIKit

class DetalheLivroViewController: UIViewController {

    var nomeLivro: String = ""
    var nomeAutor: String = ""
    var nomeEditora: String = ""
    var nomeImage: String = ""
    
    @IBOutlet weak var nome: UILabel!
    @IBOutlet weak var autor: UILabel!
    @IBOutlet weak var editora: UILabel!
    @IBOutlet weak var capa: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.nome.text = self.nomeLivro
        self.autor.text = self.nomeAutor
        self.editora.text = self.nomeEditora
        self.capa.image = UIImage(named: self.nomeImage)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
